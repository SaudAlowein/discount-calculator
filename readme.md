# Discount Calculator Application

## Overview

The application calculates discounts for existing users via a REST endpoints.

http://localhost:8080/api/v1/bills
## Prerequisites

- Docker
- Docker Compose
- Java 17
- Maven

## Getting Started

### Clone the Repository

```sh
git clone <repository-url>
cd <repository-directory>
```

### Build the application
```sh
mvn clean package
```
This will also run all tests before packaging

### Build the container
```sh
docker-compose build
```

### Run the container
```sh
docker-compose up
```

By default, the application will run on port `8080`.

### Calling the endpoint
When running locally you can use the below endpoint to for discounts:
http://localhost:8080/api/v1/bills

Example request:
```json
{
    "userId": "6665a0aec82cc0804d1cbd4c",
    "initialAmount": 240,
    "typeOfGoods": "other"
}
```

If you want to manage database users, you can use the below endpoints:

Create User:
http://localhost:8080/api/v1/users

Example request:
```json
{
    "type": "employee",
    "years": 0
}
```

List users: http://localhost:8080/api/v1/users

Get by id: http://localhost:8080/api/v1/users/{{id}}

### Environment variables
The application reads the MongoDB URI from the SPRING_DATA_MONGODB_URI environment variable.This value is to be changed if you want to connect to an external database.

### Sonarqube report
To view the sonarqube report: https://sonarcloud.io/summary/overall?id=retail-website_discounts