package com.store.discountcalculator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.store.discountcalculator.dto.request.BillRequest;
import com.store.discountcalculator.dto.response.BillResponse;
import com.store.discountcalculator.service.BillService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class BillControllerTest {

    @Mock
    private BillService billService;

    @InjectMocks
    private BillController billController;

    private MockMvc mockMvc;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(billController).build();
        objectMapper = new ObjectMapper();
    }

    @Test
    void testCalculatePayableAmount () throws Exception {
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                "other"
        );

        BillResponse billResponse = new BillResponse(
                billRequest.getInitialAmount(),
                150.0,
                350.0
        );

        final String expectedResponse = objectMapper.writeValueAsString(billResponse);

        when(billService.processBill(any(BillRequest.class))).thenReturn(billResponse);

        // When + Then
        mockMvc.perform(post("/api/v1/bills")
                .content("{\n" +
                        "    \"userId\": \"" + billRequest.getUserId() + "\",\n" +
                        "    \"initialAmount\": " + billRequest.getInitialAmount() + ",\n" +
                        "    \"typeOfGoods\": \"" + billRequest.getTypeOfGoods() + "\"\n" +
                        "}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(expectedResponse));
    }
}
