package com.store.discountcalculator.service;

import com.store.discountcalculator.constant.DateJoined;
import com.store.discountcalculator.constant.GoodsType;
import com.store.discountcalculator.constant.UserType;
import com.store.discountcalculator.dto.request.BillRequest;
import com.store.discountcalculator.dto.response.BillResponse;
import com.store.discountcalculator.exception.business.NotFoundException;
import com.store.discountcalculator.model.User;
import com.store.discountcalculator.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;


public class BillServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private BillServiceImpl billService;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testEmployeeDiscount(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                "other"
        );

        User user = new User(
                "1",
                UserType.EMPLOYEE,
                LocalDateTime.now().minusYears(DateJoined.YEARS)
        );
        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        // When
        BillResponse billResponse = billService.processBill(billRequest);

        // Then
        assertNotNull(billResponse);
        assertEquals(350.0, billResponse.getPayableAmount());
    }

    @Test
    void testAffiliateDiscount(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                "other"
        );

        User user = new User(
                "1",
                UserType.AFFILIATE,
                LocalDateTime.now().minusYears(DateJoined.YEARS)
        );
        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        // When
        BillResponse billResponse = billService.processBill(billRequest);

        // Then
        assertNotNull(billResponse);
        assertEquals(450.0, billResponse.getPayableAmount());
    }

    @Test
    void testLoyaltyDiscount(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                "other"
        );

        User user = new User(
                "1",
                "",
                LocalDateTime.now().minusYears(DateJoined.YEARS)
        );
        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        // When
        BillResponse billResponse = billService.processBill(billRequest);

        // Then
        assertNotNull(billResponse);
        assertEquals(475.0, billResponse.getPayableAmount());
    }

    @Test
    void testGroceriesDiscount(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                GoodsType.GROCERIES
        );

        User user = new User(
                "1",
                UserType.EMPLOYEE,
                LocalDateTime.now().minusYears(DateJoined.YEARS)
        );
        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        // When
        BillResponse billResponse = billService.processBill(billRequest);

        // Then
        assertNotNull(billResponse);
        assertEquals(475.0, billResponse.getPayableAmount());
    }

    @Test
    void testNoDiscount(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                50.0,
                "other"
        );

        User user = new User(
                "1",
                "",
                LocalDateTime.now()
        );
        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        // When
        BillResponse billResponse = billService.processBill(billRequest);

        // Then
        assertNotNull(billResponse);
        assertEquals(50.0, billResponse.getPayableAmount());
    }
    @Test
    void testUserNotFound(){
        // Given
        BillRequest billRequest = new BillRequest(
                "1",
                500.0,
                GoodsType.GROCERIES
        );

        when(userRepository.findById("1")).thenReturn(Optional.empty());

        // When
        NotFoundException exception = assertThrows(NotFoundException.class,
                () -> billService.processBill(billRequest));


        // Then
        assertNotNull(exception);
    }
}
