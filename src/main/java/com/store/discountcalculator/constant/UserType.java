package com.store.discountcalculator.constant;

public class UserType {
    public static final String EMPLOYEE = "employee";
    public static final String AFFILIATE = "affiliate";
}
