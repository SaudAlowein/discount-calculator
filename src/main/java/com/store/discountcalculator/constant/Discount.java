package com.store.discountcalculator.constant;

public class Discount {
    public static final Double EMPLOYEE = 0.3;
    public static final Double AFFILIATE = 0.1;
    public static final Double LOYALTY = 0.05;
    public static final Double FLAT = 5.0;
}
