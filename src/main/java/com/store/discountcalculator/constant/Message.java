package com.store.discountcalculator.constant;

public class Message {
    public static final String REQUIRED_FIELD_MISSING = "This field is required";
    public static final String USERID_NOT_FOUND = "User not found for the given id";
}
