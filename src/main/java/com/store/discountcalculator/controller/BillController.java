package com.store.discountcalculator.controller;

import com.store.discountcalculator.dto.request.BillRequest;
import com.store.discountcalculator.dto.response.BillResponse;
import com.store.discountcalculator.service.BillService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/bills")
public class BillController {

    @Autowired
    BillService billService;

    /**
     * Calculate the final payable amount after applying the highest applicable discount.
     *
     * @param billRequest the bill request containing the details for calculation
     * @return the bill response with the final payable amount
     */
    @PostMapping
    public ResponseEntity<BillResponse> calculatePayableAmount(@RequestBody @Valid BillRequest billRequest){
        return ResponseEntity.ok(billService.processBill(billRequest));
    }

}
