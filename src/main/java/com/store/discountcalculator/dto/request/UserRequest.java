package com.store.discountcalculator.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {
    private String type;
    private Long years;
}
