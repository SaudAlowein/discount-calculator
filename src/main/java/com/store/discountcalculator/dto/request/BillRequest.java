package com.store.discountcalculator.dto.request;

import com.store.discountcalculator.constant.Message;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class BillRequest {
    @NotBlank(message = Message.REQUIRED_FIELD_MISSING)
    private String userId;
    @NotNull(message = Message.REQUIRED_FIELD_MISSING)
    private Double initialAmount;
    @NotBlank(message = Message.REQUIRED_FIELD_MISSING)
    private String typeOfGoods;
}
