package com.store.discountcalculator.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Document
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private String id;
    private String type;
    private LocalDateTime dateJoined;

    public User (String type, LocalDateTime dateJoined){
        this.type = type;
        this.dateJoined = dateJoined;
    }
}
