package com.store.discountcalculator.service;

import com.store.discountcalculator.dto.request.UserRequest;
import com.store.discountcalculator.model.User;

import java.util.List;

public interface UserService {

    User create (UserRequest userRequest);

    List<User> list();

    User getById(String id);
}
