package com.store.discountcalculator.service;

import com.store.discountcalculator.dto.request.BillRequest;
import com.store.discountcalculator.dto.response.BillResponse;

public interface BillService {

    /**
     * Calculates the final bill after applying the highest applicable discount.
     *
     * @param billRequest the bill request containing the details for calculation
     * @return the bill response with the final payable amount
     */
    BillResponse processBill (BillRequest billRequest);
}
