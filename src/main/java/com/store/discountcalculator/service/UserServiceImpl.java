package com.store.discountcalculator.service;

import com.store.discountcalculator.constant.Message;
import com.store.discountcalculator.dto.request.UserRequest;
import com.store.discountcalculator.exception.business.NotFoundException;
import com.store.discountcalculator.model.User;
import com.store.discountcalculator.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;
    @Override
    public User create(UserRequest userRequest) {
        return userRepository.save(new User(
                userRequest.getType(),
                LocalDateTime.now().minusYears(userRequest.getYears())
        ));
    }

    @Override
    public List<User> list() {
        return userRepository.findAll();
    }

    @Override
    public User getById(String id){
        Optional<User> user = userRepository.findById(id);
        if (user.isEmpty()) {
            throw new NotFoundException(Message.USERID_NOT_FOUND);
        }
        return user.get();
    }
}
