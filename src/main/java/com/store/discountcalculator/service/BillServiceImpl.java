package com.store.discountcalculator.service;

import com.store.discountcalculator.constant.*;
import com.store.discountcalculator.dto.request.BillRequest;
import com.store.discountcalculator.dto.response.BillResponse;
import com.store.discountcalculator.exception.business.NotFoundException;
import com.store.discountcalculator.model.User;
import com.store.discountcalculator.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class BillServiceImpl implements BillService {
    private static final Logger logger = LoggerFactory.getLogger(BillServiceImpl.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public BillResponse processBill (BillRequest billRequest){
        Optional<User> user = userRepository.findById(billRequest.getUserId());
        if (user.isEmpty()) {
            logger.warn("User with id {} was not found", billRequest.getUserId());
            throw new NotFoundException(Message.USERID_NOT_FOUND);
        }
        logger.info("user found: {}", user.get());
        Double discount = calculateHighestDiscount(billRequest, user.get());
        return new BillResponse(
                billRequest.getInitialAmount(),
                discount,
                applyDiscount(billRequest.getInitialAmount(), discount)
        );
    }

    /**
     * Compares all possible discounts to find the highest one
     *
     * @param billRequest the bill request containing the details for calculation
     * @param user the user that is currently applying for a discount
     * @return the highest applicable discount
     */
    private Double calculateHighestDiscount(BillRequest billRequest, User user) {
        List<Double> validDiscounts = new ArrayList<>();
        validDiscounts.add(flatDiscount(billRequest.getInitialAmount()));
        if (!billRequest.getTypeOfGoods().equalsIgnoreCase(GoodsType.GROCERIES)) {
            if (user.getType().equalsIgnoreCase(UserType.EMPLOYEE)) {
                validDiscounts.add(employeeDiscount(billRequest.getInitialAmount()));
            } else if (user.getType().equalsIgnoreCase(UserType.AFFILIATE)) {
                validDiscounts.add(affiliateDiscount(billRequest.getInitialAmount()));
            }
            validDiscounts.add(loyaltyDiscount(billRequest.getInitialAmount(), user.getDateJoined()));
        }
        logger.info("Discounts List: " + validDiscounts);
        return Collections.max(validDiscounts);
    }

    private Double flatDiscount (Double initialAmount){
        Double dividedByHundred = Math.floor(initialAmount / 100);
        return dividedByHundred * Discount.FLAT;
    }

    private Double employeeDiscount (Double initialAmount){
        return initialAmount * Discount.EMPLOYEE;
    }

    private Double affiliateDiscount (Double initialAmount){
        return initialAmount * Discount.AFFILIATE;
    }

    private Double loyaltyDiscount (Double initialAmount, LocalDateTime dateJoined){
        LocalDateTime yearsToQualify = LocalDateTime.now().minusYears(DateJoined.YEARS);
        if (dateJoined.isBefore(yearsToQualify)){
            return initialAmount * Discount.LOYALTY;
        }
        return 0.0;
    }

    private Double applyDiscount(Double initialAmount, Double discountedAmount) {
        return initialAmount - discountedAmount;
    }
}
